# StrawberryFields Django

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/acefed/strawberryfields-django)

[https://acefed.gitlab.io/strawberryfields](https://acefed.gitlab.io/strawberryfields)

```shell
$ command -v bash curl docker git openssl sed
$ command -v bash curl docker git openssl sed | wc -l
6
```

```shell
$ curl -fsSL https://gitlab.com/acefed/strawberryfields-django/-/raw/master/install.sh | PORT=8080 bash -s -- https://www.example.com
$ curl https://www.example.com/
StrawberryFields Django
```

[https://acefed.gitlab.io/strawberryfields/strawberryfields-django.spdx](https://acefed.gitlab.io/strawberryfields/strawberryfields-django.spdx)  
[https://acefed.gitlab.io/strawberryfields/strawberryfields-django.json](https://acefed.gitlab.io/strawberryfields/strawberryfields-django.json)

SPDX-License-Identifier: MIT
