# StrawberryFields Django

- [3.0.0]
- [2.9.0] - 2024-08-10 - 8 files changed, 163 insertions(+), 94 deletions(-)
- [2.8.0] - 2024-03-10 - 2 files changed, 109 insertions(+), 40 deletions(-)
- [2.7.0] - 2024-02-04 - 6 files changed, 102 insertions(+), 46 deletions(-)
- [2.6.0] - 2023-11-11 - 10 files changed, 52 insertions(+), 40 deletions(-)
- [2.5.0] - 2023-11-05 - 12 files changed, 209 insertions(+), 90 deletions(-)
- [2.4.0] - 2023-04-16 - 6 files changed, 100 insertions(+), 46 deletions(-)
- [2.3.0] - 2022-11-27 - 10 files changed, 53 insertions(+), 26 deletions(-)
- [2.2.0] - 2022-11-20 - 4 files changed, 10 insertions(+), 6 deletions(-)
- [2.1.0] - 2022-06-26 - 4 files changed, 19 insertions(+), 32 deletions(-)
- [2.0.0] - 2022-03-13 - 6 files changed, 240 insertions(+), 31 deletions(-)
- [1.3.0] - 2021-10-11 - 3 files changed, 47 insertions(+), 3 deletions(-)
- [1.2.0] - 2021-10-01 - 3 files changed, 9 insertions(+), 2 deletions(-)
- [1.1.0] - 2021-09-25 - 6 files changed, 22 insertions(+), 36 deletions(-)
- 1.0.0 - 2021-04-29

[3.0.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/cd5da22a...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/7f0c304e...cd5da22a
[2.8.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/8b5cab4a...7f0c304e
[2.7.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/3cd6c31c...8b5cab4a
[2.6.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/b0225d4d...3cd6c31c
[2.5.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/c4fdfbba...b0225d4d
[2.4.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/50947748...c4fdfbba
[2.3.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/06bc4e74...50947748
[2.2.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/196d6f66...06bc4e74
[2.1.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/b7c85c7d...196d6f66
[2.0.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/ecfe0809...b7c85c7d
[1.3.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/f8f15f44...ecfe0809
[1.2.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/8d019f32...f8f15f44
[1.1.0]: https://gitlab.com/acefed/strawberryfields-django/-/compare/d4253f09...8d019f32
