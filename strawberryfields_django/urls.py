"""
URL configuration for strawberryfields_django project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path("", views.home, name="home")
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path("", Home.as_view(), name="home")
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path("blog/", include("blog.urls"))
"""

from django.urls import path
from app import views

urlpatterns = [
    path("", views.home),
    path("about", views.about),
    path("u/<username>", views.u_user),
    path("u/<username>/inbox", views.inbox),
    path("u/<username>/outbox", views.outbox),
    path("u/<username>/following", views.following),
    path("u/<username>/followers", views.followers),
    path("s/<secret>/u/<username>", views.s_send),
    path(".well-known/nodeinfo", views.nodeinfo),
    path(".well-known/webfinger", views.webfinger),
    path("@", views.u),
    path("u", views.u),
    path("user", views.u),
    path("users", views.u),
    path("users/<username>", views.uu),
    path("user/<username>", views.uu),
    path("@<username>", views.uu),
]
